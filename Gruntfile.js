module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		project: {
			banner:	'/* Built on <%= grunt.template.today("dddd, mmmm dS, yyyy, h:MM:ss TT") %> */',
			default: {
				scripts: {
					main: [
						'assets/base/scripts/jquery.js',
						'assets/base/scripts/jquery.maskedinput.js',
						'assets/base/scripts/jquery.caroufredsel.js',
						'assets/default/scripts/basis.js',
						'assets/default/scripts/basis.form.js',
						'assets/default/scripts/basis.alert.js',
						'assets/default/scripts/basis.collapse.js',
						'assets/default/scripts/basis.togglebox.js',
						'assets/default/scripts/basis.slider.js',
						'assets/default/scripts/basis.init.js',
						'assets/default/scripts/main.js'
					],
					init: [
						'assets/base/scripts/modernizr.js',
						'assets/default/scripts/init.js'
					]
				},
				styles: {
					path: 'assets/default/styles',
					compile: '<%= project.default.styles.path %>/**/*.sass'
				}
			}
		},

		jshint: {
			options: {
				force: true
			},
			default_main: ['assets/default/scripts/**/*', '!assets/default/scripts/init.js'],
			default_init: ['assets/default/scripts/init.js']
		},

		uglify: {
			options: {
				banner:	'<%= project.banner %>\n'
			},
			default_main: {
				src: '<%= project.default.scripts.main %>',
				dest: 'deploy/assets/default/scripts/main.js'
			},
			default_init: {
				src: '<%= project.default.scripts.init %>',
				dest: 'deploy/assets/default/scripts/init.js'
			}
		},

		compass: {
			options: {
				banner:	'<%= project.banner %>',
				relativeAssets: true,
				noLineComments: true,
				outputStyle: 'compressed',
				raw: 'preferred_syntax = :sass'
			},
			default: {
				options: {
					sassDir: '<%= project.default.styles.path %>',
					specify: '<%= project.default.styles.compile %>',
					cssDir: 'deploy/assets/default/styles',
					imagesDir: 'deploy/assets/default/images',
					javascriptsDir: 'deploy/assets/default/scripts',
					fontsDir: 'deploy/assets/default/fonts'
				}
			}
		},

		watch: {
			options: {
				livereload: true
			},
			grunt: {
				files: ['Gruntfile.js'],
				tasks: ['default']
			},
			default_scripts_main: {
				files: ['<%= project.default.scripts.main %>'],
				tasks: ['jshint:default_main', 'uglify:default_main']
			},
			default_scripts_init: {
				files: ['<%= project.default.scripts.init %>'],
				tasks: ['jshint:default_init', 'uglify:default_init']
			},
			default_styles: {
				files: ['<%= project.default.styles.compile %>'],
				tasks: ['compass:default']
			},
			app: {
				files: ['deploy/app/**/*']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['jshint', 'uglify', 'compass']);
};
