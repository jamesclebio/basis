<?php
class Index extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('index');
		$this->setTitle('');
		$this->setDescription('');
		$this->setAnalytics(true);
	}
}
