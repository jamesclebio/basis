<?php
class Contact extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('contact');
		$this->setTitle('');
		$this->setDescription('');
		$this->setAnalytics(true);
		$this->form();
	}

	public function form() {
		if ($_POST) {
			date_default_timezone_set('America/Sao_Paulo');

			$mail = new MailHelper();

			$mail->setHeader(
				'From: ' . $_POST['name'] . ' <' . $_POST['email'] . '>' . "\r\n" .
				'Reply-To: ' . $_POST['email'] . "\r\n"
			);
			$mail->setTo('dev@jamesclebio.com.br');
			$mail->setSubject('[Site] Contato');
			$mail->setBody(
				'Nome: ' . $_POST['name'] . "\n" .
				'E-mail: ' . $_POST['email'] . "\n" .
				'Telefone: ' . $_POST['phone'] . "\n\n" .
				'---' . "\n\n" .
				$_POST['message'] . "\n\n" .
				'---' . "\n\n" .
				'Enviado a partir de ' . $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI'] . ' em ' . date('d/m/Y H:i:s')
			);
			$mail->setAlertSuccess('
				<p><strong>Sua mensagem foi enviada com sucesso!</strong></p>
				<p>Daremos um retorno o mais breve possível.</p>
			');
			$mail->send($this);
		}
	}
}
