<form id="form-contact" method="post" class="form">
	<fieldset>
		<legend>Contato</legend>
		<div class="grid grid-items-2">
			<div class="grid-item">
				<label>Nome *<input name="name" type="text" required></label>
				<label>E-mail *<input name="email" type="email" required></label>
				<label>Telefone *<input name="phone" type="text" class="mask-phone" required></label>
			</div>

			<div class="grid-item">
				<label>Mensagem *<textarea name="message" id="" cols="30" rows="10" class="height-195" required></textarea></label>
			</div>
		</div>

		<div class="panel-action">
			<button type="submit" class="button button-info">Enviar</button>
		</div>
	</fieldset>
</form>
