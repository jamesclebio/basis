<?php
/**
 * Basis
 *
 * @author James Clébio <jamesclebio@gmail.com>
 * @link https://github.com/jamesclebio/basis 
 * @license http://opensource.org/licenses/MIT MIT License
 */

class Autoload
{
	static public function core($class) {
		$file = CORE . $class . '.php';

		if (file_exists($file))
			require_once($file);
	}

	static public function model($class) {
		$file = MODELS . $class . '.php';

		if (file_exists($file))
			require_once($file);
	}

	static public function helper($class) {
		$file = HELPERS . $class . '.php';

		if (file_exists($file))
			require_once($file);
	}
}
