<?php
/**
 * Basis
 *
 * @author James Clébio <jamesclebio@gmail.com>
 * @link https://github.com/jamesclebio/basis 
 * @license http://opensource.org/licenses/MIT MIT License
 */

class Page
{
	private $name;
	private $root;
	private $get;
	private $alert;
	private $layout = 'default';
	private $view = 'index';
	private $title;
	private $description;
	private $html_attribute;
	private $html_class;
	private $head_append;
	private $body_attribute;
	private $body_class;
	private $body_prepend;
	private $body_append;
	private $analytics = false;

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setRoot($root) {
		$this->root = $root;
	}

	public function setGet($get) {
		$this->get = $get;
	}

	protected function getAlert() {
		echo $this->alert;
	}

	public function setAlert($content = null, $type = 'warning') {
		if ($content != null)
			$this->alert = '<div class="alert alert-' . $type . '"><a href="#" class="close" title="Fechar alerta">x</a>' . $content . '</div>';
	}

	public function getLayout() {
		if ($this->layout)
			require_once LAYOUTS . $this->layout . '.php';
	}

	protected function setLayout($layout) {
		$this->layout = $layout;
	}

	public function getView() {
		if ($this->view)
			require_once VIEWS . $this->view . '.php';
	}

	protected function setView($view) {
		$this->view = $view;
	}

	protected function getTitle() {
		return $this->title;
	}

	protected function setTitle($title) {
		$this->title = $title;
	}

	protected function getDescription() {
		return $this->description;
	}

	protected function setDescription($description) {
		$this->description = $description;
	}

	protected function getHtmlAttribute() {
		echo ' ', $this->html_attribute;
	}

	protected function setHtmlAttribute($html_attribute) {
		$this->html_attribute = $html_attribute;
	}

	protected function getHtmlClass() {
		echo $this->html_class;
	}

	protected function setHtmlClass($html_class) {
		$this->html_class = $html_class;
	}

	protected function getBodyAttribute() {
		echo ' ', $this->body_attribute;
	}

	protected function setBodyAttribute($body_attribute) {
		$this->body_attribute = $body_attribute;
	}

	protected function getBodyClass() {
		echo $this->body_class;
	}

	protected function setBodyClass($body_class) {
		$this->body_class = $body_class;
	}

	protected function getHeadAppend() {
		echo $this->head_append;
	}

	protected function setHeadAppend($head_append) {
		$this->head_append = $head_append;
	}

	protected function getBodyPrepend() {
		echo $this->body_prepend;
	}

	protected function setBodyPrepend($body_prepend) {
		$this->body_prepend = $body_prepend;
	}

	protected function getBodyAppend() {
		echo $this->body_append;
	}

	protected function setBodyAppend($body_append) {
		$this->body_append = $body_append;
	}

	protected function getAnalytics() {
		if ($this->analytics)
			include 'analytics.php';
	}

	protected function setAnalytics($analytics) {
		$this->analytics = $analytics;
	}

	protected function _get($index = null) {
		if ($index != null)
			return (array_key_exists($index, $this->get)) ? $this->get[$index] : null;
		else
			return $this->get;
	}

	protected function _url($target = null) {
		return ($target != 'root') ? $this->root . $target : $this->root;
	}

	protected function _asset($target = null) {
		return $this->root . ASSETS . $target;
	}

	protected function _include($target = null) {
		return $this->root . INCLUDES . $target;
	}

	protected function cacheInstance() {
		file_put_contents(CACHE . 'instance-page-' . $this->name, serialize($this));
	}
}
