window.basis = {
	about: {
		name: 'Basis',
		version: '0.0.1',
		release: 'November 28th, 2013',
		author: 'James Clébio <jamesclebio@gmail.com>',
		license: 'MIT'
	}
};
