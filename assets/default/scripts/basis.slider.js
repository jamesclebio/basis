;(function($, window, document, undefined) {
	'use strict';

	basis.slider = {
		wrapper: '.slider',

		build: function() {
			var	$wrapper = $(basis.slider.wrapper),
				template_prevnext = '<div class="nav"><a href="#" class="prev">prev</a><a href="#" class="next">next</a></div>',
				progress = false;

			$wrapper.append(template_prevnext);

			if ($wrapper.find('.container .item').length > 1) {
				$wrapper.append('<div class="progress"></div>');
				progress = $wrapper.find('.progress');
			}

			$wrapper.find('.container').carouFredSel({
				items: {
					visible: 1,
					minimum: 2
				},
				scroll: {
					fx: 'crossfade'
				},
				auto: {
					timeoutDuration: 4000,
					progress: progress
				},
				prev: $wrapper.find('.nav .prev'),
				next: $wrapper.find('.nav .next'),
				responsive: true
			});
		},

		init: function() {
			var	$wrapper = $(this.wrapper);

			if ($wrapper.length) {
				this.build();
			}
		}
	};
}(jQuery, this, this.document));
