;(function($, window, document, undefined) {
	'use strict';

	basis.collapse = {
		wrapper: '.collapse',

		bindings: function() {
			var	$wrapper = $(basis.collapse.wrapper);

			$wrapper.find('.collapse-heading a').on({
				click: function(e) {
					var	$collapse = $(this).closest(basis.collapse.wrapper),
						class_open = 'collapse-open';

					$collapse.find('.collapse-container').fadeToggle(100, function() {
						if ($(this).is(':visible'))
							$collapse.addClass(class_open);
						else
							$collapse.removeClass(class_open);
					});

					e.preventDefault();
				}
			});
		},

		init: function() {
			var	$wrapper = $(this.wrapper);

			if ($wrapper.length) {
				this.bindings();
			}
		}
	};
}(jQuery, this, this.document));
