;(function($, window, document, undefined) {
	'use strict';

	basis.alert = {
		wrapper: '.alert',
		template_close: '<a href="#" class="close" title="Fechar alerta">x</a>',

		bindings: function() {
			$(document).on({
				click: function(e) {
					$(this).closest(basis.alert.wrapper).fadeOut(100, function() {
						$(this).remove();
					});

					e.preventDefault();
				}
			}, basis.alert.wrapper + ' .close');
		},

		init: function() {
			this.bindings();
		}
	};
}(jQuery, this, this.document));
