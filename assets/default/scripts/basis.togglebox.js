;(function($, window, document, undefined) {
	'use strict';

	basis.togglebox = {
		wrapper: '.togglebox',

		build: function() {
			var	$wrapper = $(this.wrapper),
				box_title;

			$wrapper.each(function() {
				box_title = $(this).data('boxTitle');

				if (box_title !== undefined) {
					$(this).prepend('<div class="togglebox-trigger"><a href="#"></a></div>');

					if ($(this).hasClass('togglebox-open'))
						$(this).find('.togglebox-trigger a').text('Ocultar ' + box_title);
					else
						$(this).find('.togglebox-trigger a').text('Mostrar ' + box_title);
				}
			});
		},

		bindings: function() {
			var	$wrapper = $(this.wrapper);

			$wrapper.on({
				click: function(e) {
					var	$this = $(this),
						$togglebox = $(this).closest(basis.togglebox.wrapper),
						box_title = $togglebox.data('boxTitle'),
						class_open = 'togglebox-open';

					$togglebox.find('.togglebox-container').fadeToggle(100, function() {
						if ($(this).is(':visible')) {
							$this.text('- ' + box_title);
							$togglebox.addClass(class_open);

						} else {
							$this.text('+ ' + box_title);
							$togglebox.removeClass(class_open);
						}
					});

					e.preventDefault();
				}
			}, '.togglebox-trigger a');
		},

		init: function() {
			var	$wrapper = $(this.wrapper);

			if ($wrapper.length) {
				this.build();
				this.bindings();
			}
		}
	};
}(jQuery, this, this.document));
