;(function($, window, document, undefined) {
	'use strict';

	basis.init = function() {
		this.form.init();
		this.alert.init();
		this.collapse.init();
		this.togglebox.init();
		this.slider.init();
	};

	basis.init();
}(jQuery, this, this.document));
